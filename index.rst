.. Kailand Gateway documentation master file, created by
   sphinx-quickstart on Sat May 21 21:22:35 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kailand Gateway
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   README
   Hobbies
   Checklists
   Resources

Indices and tables:

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

