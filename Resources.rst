=========
Resources
=========

- `小额法庭告Homedepot <https://drive.google.com/file/d/1fHTY-oJd7W4mskg6V6itdI6My82RPnGvp7RD5ESq42nbnadH0UUf99hhyy7J9zrVODKsq7lPa-d_cLxh/view?usp=sharing>`_, from mitbbs.com

- `A Python API for LendingClub <https://github.com/jgillick/LendingClub>`_.

- `Boost Shared Pointer <http://www.boost.org/doc/libs/1_60_0/libs/smart_ptr/shared_ptr.htm>`_.